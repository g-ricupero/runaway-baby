\include "include/global.ly"
\include "include/harmony.ly"
\include "include/outline.ly"
\include "include/alto.ly"

\header {
	instrument = \markup \italic \normalsize {
		\override #'(font-name . "Arial")
		"Alto Sax Eb"
	}
}

\score {
	\transpose c a <<
		% \new ChordNames {
		% 	\set chordChanges = ##t
		% 	\harmony
		% }
		\new Staff <<
			\clef treble
			\NoChords \global \outline \alto
		>>
	>>
	\layout {}
}
