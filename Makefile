SONG=Runaway Baby
COMPILER=lilypond

score:
	@${COMPILER} score.ly
	rm -f score.ps
midi_score:
	@${COMPILER} midi/score.ly
	mv score.midi "midi/${SONG}.midi"
	aoss timidity -A100a -in "midi/${SONG}.midi"
tp:
	@${COMPILER} tp.ly
	rm -f tp.ps
midi_tp:
	@${COMPILER} midi/tp.ly
	mv tp.midi "midi/${SONG}_tp.midi"
	aoss timidity -A100a -in "midi/${SONG}_tp.midi"
as:
	@${COMPILER} as.ly
	rm -f as.ps
midi_as:
	@${COMPILER} midi/as.ly
	mv as.midi "midi/${SONG}_as.midi"
	aoss timidity -A100a -in "midi/${SONG}_as.midi"
ts:
	@${COMPILER} ts.ly
	rm -f ts.ps
midi_ts:
	@${COMPILER} midi/ts.ly
	mv ts.midi "midi/${SONG}_ts.midi"
	aoss timidity -A100a -in "midi/${SONG}_ts.midi"
tb:
	@${COMPILER} tb.ly
	rm -f tb.ps
midi_tb:
	@${COMPILER} midi/tb.ly
	mv tb.midi "midi/${SONG}_tb.midi"
	aoss timidity -A100a -in "midi/${SONG}_tb.midi"
bs:
	@${COMPILER} bs.ly
	rm -f bs.ps
midi_bs:
	@${COMPILER} midi/bs.ly
	mv bs.midi "midi/${SONG}_bs.midi"
	aoss timidity -A100a -in "midi/${SONG}_bs.midi"
rhythm:
	@${COMPILER} rhythm.ly
	rm -f rhythm.ps
midi_rhythm:
	@${COMPILER} midi/rhythm.ly
	mv rhythm.midi "midi/${SONG}_rhythm.midi"
	aoss timidity -A100a -in "midi/${SONG}_rhythm.midi"
ebass:
	@${COMPILER} ebass.ly
	rm -f ebass.ps
midi_ebass:
	@${COMPILER} midi/ebass.ly
	mv ebass.midi "midi/${SONG}_ebass.midi"
	aoss timidity -A100a -in "midi/${SONG}_ebass.midi"
mp3: wav
	@lame -h "wav/${SONG}.wav" "mp3/${SONG}.mp3"
	lame -h "wav/${SONG}_tp.wav" "mp3/${SONG}_tp.mp3"
	lame -h "wav/${SONG}_as.wav" "mp3/${SONG}_as.mp3"
	lame -h "wav/${SONG}_ts.wav" "mp3/${SONG}_ts.mp3"
	lame -h "wav/${SONG}_tb.wav" "mp3/${SONG}_tb.mp3"
	lame -h "wav/${SONG}_bs.wav" "mp3/${SONG}_bs.mp3"
	lame -h "wav/${SONG}_ebass.wav" "mp3/${SONG}_ebass.mp3"
wav: midi
	@timidity -Ow "midi/${SONG}.midi"
	mv "midi/${SONG}.wav" wav
	timidity -Ow "midi/${SONG}_tp.midi"
	mv "midi/${SONG}_tp.wav" wav
	timidity -Ow "midi/${SONG}_as.midi"
	mv "midi/${SONG}_as.wav" wav
	timidity -Ow "midi/${SONG}_ts.midi"
	mv "midi/${SONG}_ts.wav" wav
	timidity -Ow "midi/${SONG}_tb.midi"
	mv "midi/${SONG}_tb.wav" wav
	timidity -Ow "midi/${SONG}_bs.midi"
	mv "midi/${SONG}_bs.wav" wav
	timidity -Ow "midi/${SONG}_ebass.midi"
	mv "midi/${SONG}_ebass.wav" wav
midi: audio
	@mv score.midi "midi/${SONG}.midi"
	mv tp.midi "midi/${SONG}_tp.midi"
	mv as.midi "midi/${SONG}_as.midi"
	mv ts.midi "midi/${SONG}_ts.midi"
	mv tb.midi "midi/${SONG}_tb.midi"
	mv bs.midi "midi/${SONG}_bs.midi"
	mv ebass.midi "midi/${SONG}_ebass.midi"
audio:
	@${COMPILER} midi/score.ly
	${COMPILER} midi/tp.ly
	${COMPILER} midi/as.ly
	${COMPILER} midi/ts.ly
	${COMPILER} midi/tb.ly
	${COMPILER} midi/bs.ly
	${COMPILER} midi/ebass.ly

all: score
all: as
all: ts
all: tb
all: bs
all: tp
all: rhythm
all: ebass
all: midi

clean:
	@rm -f *.pdf midi/*.midi wav/*.wav
cleanall:
	@rm -f *.pdf midi/*.midi wav/*.wav mp3/*.mp3
