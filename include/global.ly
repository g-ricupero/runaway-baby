\version "2.18.2"

\paper {
	#(set-paper-size "a4")          % paper size
	% ragged-last-bottom = ##f      % fill the page till bottom
	% page-count = 1                % force to render just in 1 page
	% between-system-padding = 0    % empty padding between lines
}

NoChords = {
	\override Score.MetronomeMark #'padding = #4
}
WithChords = {
	\override Score.MetronomeMark #'padding = #8
}

\header {
	title = \markup \center-align {
		\override #'(font-name . "Purisa")
		\fontsize #4 \bold
		"Runaway Baby"
	}
	subsubtitle = \markup \center-align {
		\override #'(font-name . "Arial")
		"Live in Paris 2012 version"
	}
	composer=\markup \center-align {
		\override #'(font-name . "Arial")
		"Mars, Lawrence, Levine"
	}
	opus = \markup \tiny {
		\override #'(font-name . "Arial")
		"(2010)"
	}
	tagline = \markup {
		\override #'(font-name . "Arial")
		\tiny \column {
			\fill-line { "Transcription" \italic { "Giuseppe Ricupero" } }
			\fill-line { "Updated" \italic { "09-05-2018 18.36" } }
		}
	}
}

global = {
	\time 4/4
	\key bbes \minor
	\tempo 4 = 180
	\set Score.skipBars = ##t
	\set Score.countPercentRepeats = ##t
	\set Staff.hairpinToBarline = ##f
}
